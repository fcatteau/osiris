# This file is a template, and might need editing before it works on your project.
FROM ruby:2.7-alpine

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

#CMD ["bundle", "exec", "rails", "server"]

CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
